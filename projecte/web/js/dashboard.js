/*
 * 
 * This is a file used only for the main dashboard
 *
 */
 
	// pasarle dos parametros: id para el dato en concreto (0 = agua, 1 = temp, 2 = hum, 3 = luz) y el tiempo (24h, 1w [168h], 1m [720h])
	// Carga temperatura de temperatura.php (por defecto los últimos 24 registros/horas)
	var loadTemperatura = function(temps) {
		 var jsonTemperatura;
		console.log("aaa " + temps);
		$.ajax({
			url: "temperatura.php",
			data: { 'temps':temps },
			async: false,
			type: 'POST',
			success: function(data){
				obj = JSON.parse(data);
				jsonTemperatura = obj;
			}
		});
		return jsonTemperatura;
	}; 
	
	// Carga la última lectura de temperatura para mostrar en el icono correspondiente
	var ultimaTemperatura = function() {
		var jsonTemp = loadTemperatura(1);
		
		// Obtener última posición del JSON
		//var ultimaPos = Object.keys(jsonTemp).length - 1;
		
		// Obtener último elemento del JSON
		//var highest = jsonTemp[ultimaPos];
		
		$("#ultimaTemp").html(jsonTemp[0].item1 + '<sup style="font-size: 20px">ºC</sup>');
	}
		
	$(function () {
		ultimaTemperatura();
	"use strict";
	
	$('a[href="#revenue-chart"]').click(function(){
		area.setData(loadTemperatura($(this).attr('id')));
		//area.setData(loadHumitat($(this).attr('id')));
	});	

	
  //Make the dashboard widgets sortable Using jquery UI
  $(".connectedSortable").sortable({
    placeholder: "sort-highlight",
    connectWith: ".connectedSortable",
    handle: ".box-header, .nav-tabs",
    forcePlaceholderSize: true,
    zIndex: 999999
  });
  $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");

  //jQuery UI sortable for the todo list
  $(".todo-list").sortable({
    placeholder: "sort-highlight",
    handle: ".handle",
    forcePlaceholderSize: true,
    zIndex: 999999
  });

  //bootstrap WYSIHTML5 - text editor
  $(".textarea").wysihtml5();

  $('.daterange').daterangepicker({
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment()
  }, function (start, end) {
    window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
  });
  
  //The Calender
  $("#calendar").datepicker();

  /* Morris.js Chart */
  // Gráfico temperatura
  var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    data: loadTemperatura(24),
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Temperatura'],
    lineColors: ['#a0d0e0'],
    hideHover: 'auto'
  });

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
  });

  /* The todo list plugin */
  $(".todo-list").todolist({
    onCheck: function (ele) {
      window.console.log("The element has been checked");
      return ele;
    },
    onUncheck: function (ele) {
      window.console.log("The element has been unchecked");
      return ele;
    }
  });

});
