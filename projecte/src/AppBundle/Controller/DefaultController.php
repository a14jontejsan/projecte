<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Reles;
use AppBundle\Entity\Alarma;
use AppBundle\Entity\Event;
use Symfony\Component\Validator\Constraints\DateTime;

class DefaultController extends Controller
{
    /**
    * @Route("/createEvent", name="createEvent")
     */
    public function createEventAction(Request $request)
    {
        $event = new Event();
		
		$em = $this->getDoctrine()->getManager();
		$qb = $em ->createQueryBuilder();
		
		$repository = $this->getDoctrine()->getRepository('AppBundle:Event');
		$events = $repository->findAll();
		$e = array();
		foreach($events as $a){
			array_push($e,$a);
		}
		
        $form = $this->createFormBuilder($event)
            ->add('id', EntityType::class, array('class'=>'AppBundle:Event','choice_label' => 'id', 'choices' => $e))
            ->add('data', DateTimeType::class)
            ->add('state', ChoiceType::class, array('choices'  => array('Encendre' => true,'Apagar' => false,)))
            ->add('save', SubmitType::class, array('label' => 'Crear Esdeveniment'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $result=$em->getRepository('AppBundle:Event')->findOneById($event->getId());
            if(!$result){
				throw $this->createNotFoundException(
				'No se a encontrado el Evento '); 
			}
			$result->setData($event->getData());
            $result->setState($event->getState());
            $em->flush();
            return $this->render('default/successevent.html.twig');
        }
        return $this->render('default/alarma.html.twig', array(
            'form' => $form->createView(),
        ));
	}
    /**
    * @Route("/createAlarma", name="createAlarma")
     */
    public function createAlarmaAction(Request $request)
    {
        $alarma = new Alarma();
		
		$em = $this->getDoctrine()->getManager();
		$qb = $em ->createQueryBuilder();
		
		$repository = $this->getDoctrine()->getRepository('AppBundle:Alarma');
		$alarmas = $repository->findAll();
		$alarms = array();
		foreach($alarmas as $a){
			array_push($alarms,$a);
		}
		
        $form = $this->createFormBuilder($alarma)
            ->add('id', EntityType::class, array('class'=>'AppBundle:Alarma','choice_label' => 'id', 'choices' => $alarms))
            ->add('temperatura', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Crear Alarma'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $result=$em->getRepository('AppBundle:Alarma')->findOneById($alarma->getId());
            if(!$result){
				throw $this->createNotFoundException(
				'No se a encontrado la Alarma '); 
			}
			$result->setTemperatura($alarma->getTemperatura());
            
            $em->flush();
            return $this->render('default/successalarma.html.twig');
        }
        return $this->render('default/alarma.html.twig', array(
            'form' => $form->createView(),
        ));
	}
     /**
     * @Route("/temp")
     */
    public function pruebaAction(Request $request)
    {
		return $this->render('default/pruebatemp.html.twig');
    }
    /**
    /**
     * @Route("/encender", name="encender")
     */
public function encendreActionName(Request $request){
    $em = $this->getDoctrine()->getManager();
    $allreles=$em->getRepository('AppBundle:Reles')->findAll();
    if ($request->request->has('encender1'))
    {
		$script='./scripts/open1.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(1);
			$rele->setStatus(true);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}

		echo $process->getOutput();
    }else if ($request->request->has('apagar1')){
		$script='./scripts/close1.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(1);
			$rele->setStatus(false);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}
	}
    if ($request->request->has('encender2'))
    {
		$script='./scripts/open2.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(2);
			$rele->setStatus(true);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}

		echo $process->getOutput();
    }else if ($request->request->has('apagar2')){
		$script='./scripts/close2.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(2);
			$rele->setStatus(false);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}
	}
    if ($request->request->has('encender3'))
    {
		$script='./scripts/open3.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(3);
			$rele->setStatus(true);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}

		echo $process->getOutput();
    }else if ($request->request->has('apagar3')){
		$script='./scripts/close3.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(3);
			$rele->setStatus(false);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}
	}
    if ($request->request->has('encender4'))
    {
		$script='./scripts/open4.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(4);
			$rele->setStatus(true);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}

		echo $process->getOutput();
    }else if ($request->request->has('apagar4')){
		$script='./scripts/close4.sh';
		$process = new Process($script);
		$process->run();

		// executes after the command finishes
		if (!$process->isSuccessful()) {
			throw new ProcessFailedException($process);
		}else{
			$rele= new Reles;
			$rele->setId(4);
			$rele->setStatus(false);
			$em = $this->getDoctrine()->getManager();
			$reles=$em->getRepository('AppBundle:Reles')->findOneById($rele->getId());
			if(!$reles){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$rele->getId()); 
			}
			$reles->setStatus($rele->getStatus());
			$em->flush();
			
		}
	}

return $this->render('default/index.html.twig', array(
            'allreles' => $allreles
            ));
	
}
	/**
    /**
     * @Route("/resetAlarma", name="resetAlarma")
     */
public function resetActionName(Request $request){
    
    if ($request->request->has('reset1'))
    {
			$alarma= new Alarma;
			$alarma->setId(1);
			$alarma->setTemperatura(999);
			$em = $this->getDoctrine()->getManager();
			$alarmas=$em->getRepository('AppBundle:Alarma')->findOneById($alarma->getId());
			if(!$alarmas){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$alarma->getId()); 
			}
			$alarmas->setTemperatura($alarma->getTemperatura());
			$em->flush();
			
		}
	if ($request->request->has('reset2'))
    {
			$alarma= new Alarma;
			$alarma->setId(2);
			$alarma->setTemperatura(999);
			$em = $this->getDoctrine()->getManager();
			$alarmas=$em->getRepository('AppBundle:Alarma')->findOneById($alarma->getId());
			if(!$alarmas){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$alarma->getId()); 
			}
			$alarmas->setTemperatura($alarma->getTemperatura());
			$em->flush();
			
		}
		if ($request->request->has('reset3'))
    {
			$alarma= new Alarma;
			$alarma->setId(3);
			$alarma->setTemperatura(999);
			$em = $this->getDoctrine()->getManager();
			$alarmas=$em->getRepository('AppBundle:Alarma')->findOneById($alarma->getId());
			if(!$alarmas){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$alarma->getId()); 
			}
			$alarmas->setTemperatura($alarma->getTemperatura());
			$em->flush();
			
		}
		if ($request->request->has('reset4'))
    {
			$alarma= new Alarma;
			$alarma->setId(4);
			$alarma->setTemperatura(999);
			$em = $this->getDoctrine()->getManager();
			$alarmas=$em->getRepository('AppBundle:Alarma')->findOneById($alarma->getId());
			if(!$alarmas){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$alarma->getId()); 
			}
			$alarmas->setTemperatura($alarma->getTemperatura());
			$em->flush();
			
		}

		

return $this->render('default/resetAlarma.html.twig');
	
}
/**
    /**
     * @Route("/resetEvent", name="resetEvent")
     */
public function resetEventActionName(Request $request){
    
    if ($request->request->has('reset1'))
    {
			$em = $this->getDoctrine()->getManager();
			$event=$em->getRepository('AppBundle:Event')->find(1);
			
			if(!$event){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$event->getId()); 
			}
			
			$data = new \DateTime("2100-01-01 00:00:01");

			$event->setData($data);

			$em->flush();
			
		}
	if ($request->request->has('reset2'))
    {
			$em = $this->getDoctrine()->getManager();
			$event=$em->getRepository('AppBundle:Event')->find(2);
			
			if(!$event){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$event->getId()); 
			}
			
			$data = new \DateTime("2100-01-01 00:00:01");

			$event->setData($data);

			$em->flush();
			
		}
		if ($request->request->has('reset3'))
    {
			$em = $this->getDoctrine()->getManager();
			$event=$em->getRepository('AppBundle:Event')->find(3);
			
			if(!$event){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$event->getId()); 
			}
			
			$data = new \DateTime("2100-01-01 00:00:01");

			$event->setData($data);

			$em->flush();
			
		}
		if ($request->request->has('reset4'))
    {
			
			$em = $this->getDoctrine()->getManager();
			$event=$em->getRepository('AppBundle:Event')->find(4);
			
			if(!$event){
				throw $this->createNotFoundException(
				'No se a encontrado el Rele '.$event->getId()); 
			}
			
			$data = new \DateTime("2100-01-01 00:00:01");

			$event->setData($data);

			$em->flush();
			
		}

		

return $this->render('default/resetEvent.html.twig');
	
}

}
