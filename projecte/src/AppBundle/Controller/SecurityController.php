<?php
// src/AppBundle/Controller/SecurityController.php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;

class SecurityController extends Controller
{
/**
     * @Route("/", name="homepage")
     */
    public function loginAction(Request $request)
{
		$em = $this->getDoctrine()->getManager();
		$allreles=$em->getRepository('AppBundle:Reles')->findAll();
        $repository =$em->getRepository('AppBundle:User');

        if($request->getMethod()=='POST')
        {
            $username=$request->get('username');
            
            $password=$request->get('password');
            $password=sha1($password);
            

            $user=$repository->findOneBy(array('username'=>$username,'password'=>$password));

            if ($user) //if user has values
                return $this->render('default/index.html.twig', array(
            'allreles' => $allreles
            ));

            else//if login is incorrect
                return $this->render('security/login.html.twig');
        }


        return $this->render('security/login.html.twig');
}
}
