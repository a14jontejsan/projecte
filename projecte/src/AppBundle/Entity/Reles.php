<?php
/// src/AppBundle/Entity/Reles.php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="reles")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\RelesRepository")
 */
 
class Reles
{
	/**
	 * 
	 * @var int
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    protected $id;
    
    /**
     * @ORM\Column(name="status", type="boolean", length=100)
     */
    protected $status;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Reles
     */

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Reles
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}
