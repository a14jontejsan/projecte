<?php
/// src/AppBundle/Entity/Alarma.php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="alarma")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AlarmaRepository")
 */
 
class Alarma
{
	/**
	 * 
	 * @var int
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    protected $id;
    
    /**
     * @ORM\Column(name="temperatura", type="string")
     */
    protected $temperatura;

    /**
     * Set rele
     *
     * @param integer $rele
     *
     * @return Alarma
     */
    public function setRele($rele)
    {
        $this->rele = $rele;

        return $this;
    }

    /**
     * Get rele
     *
     * @return integer
     */
    public function getRele()
    {
        return $this->rele;
    }

    /**
     * Set temperatura
     *
     * @param float $temperatura
     *
     * @return Alarma
     */
    public function setTemperatura($temperatura)
    {
        $this->temperatura = $temperatura;

        return $this;
    }

    /**
     * Get temperatura
     *
     * @return float
     */
    public function getTemperatura()
    {
        return $this->temperatura;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Alarma
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
