<?php
/// src/AppBundle/Entity/Event.php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\EventRepository")
 */
 
class Event
{
	/**
	 * 
	 * @var int
	 * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    protected $id;
    
    /**
     * @ORM\Column(name="data", type="datetime")
     */
    protected $data;
    /**
     * @ORM\Column(name="state", type="boolean")
     */
    protected $state;

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Event
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Event
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set state
     *
     * @param boolean $state
     *
     * @return Event
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return boolean
     */
    public function getState()
    {
        return $this->state;
    }
}
